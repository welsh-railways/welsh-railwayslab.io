Mid Wales Line
=============================

This route fills the missing links north to south


<a href="mid_wales_line.png"><img src="mid_wales_line.png"></a>


<iframe width="560" height="315" src="https://www.youtube.com/embed/8UzTPj7dz_c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## New Track Sections

- Summary of new sections required
- Around **70** miles in total
- Target speed of 100mph+

### Merthyr <> Brecon 

Continues from Merthyr station up and over to Brecon

- 18.2 miles (15 mins)
- Merthyr: 170m / 570ft
- Brecon: 135m / 440ft
- A significant project but potentially a senic one also

<a href="merthyr-brecon.png"><img height="300" src="merthyr-brecon.png"></a>


### Brecon <> Builth Wells

Continues from Brecon, wending its way through
the valleys before stopping at the Royal Welsh. Continues on to join
the Heart of Wales line at Builth Road
 
- 15.6 miles (12 mins)
- Brecon: 135m / 440ft
- Builth Wells: 125m / 410ft
- Profile is pretty flat with valley hugging

<a href="brecon-builth.png"><img height="300" src="brecon-builth.png"></a>

### Llandrindod <> Newtown 

Connects the Heart of Wales at Llandrindod to the Machynlleth line at Newtown 

- 20 miles (13 mins)
- Llandrindod: 210m / 690ft
- Newtown: 116m / 380ft
- Profile is pretty flat with valley hugging, apart from descending to Newtown

<a href="llandrindod-newtown.png"><img height="300" src="llandrindod-newtown.png"></a>

### Welshpool <> Oswestry 

Reinstate the Beeching line for 100mph 

- 15 miles (10 mins)
- Profile is there already and its pretty flat
- Possible new stations at Four Crosses and Pant


<a href="welshpool-oswestry.png"><img height="300" src="welshpool-oswestry.png"></a>



## Other Stuff
- Download <a href="https://welsh-railways.gitlab.io/mid-wales-line/mid-wales-line.kml">KML File</a>
- [reddit/wales](https://www.reddit.com/r/Wales/comments/f9yl05/new_mid_wales_line_the_missing_links/)





