Anglesey Central Railway
============================

- 17.2 miles
- Opened: 1870's
- Closed: 1964 Beeching Axe
- [Wikipedia](https://en.wikipedia.org/wiki/Anglesey_Central_Railway)


<a href="anglesey-central.png"><img src="anglesey-central.png"></a>

- Download <a href="https://welsh-railways.gitlab.io/anglesey-central/anglesey-central-railway.kml">KML File</a>