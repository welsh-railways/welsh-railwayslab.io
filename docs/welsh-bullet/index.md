Welsh Bullet Train
==========================

What if Wales had a Japanese style bullet train, a [Shinkasen](https://en.wikipedia.org/wiki/Shinkansen) (new trunk line).

- Grade seperated
- Viaducts over fields, and roads into city centers
- Tunnels straight through mountains and under estuaries
- A line speed of 160mph+ (2.5+ miles per minute)
- Abertwyth to Cardiff would be around 1.15 hours
  
<a href="welsh-bullet-train.png"><img src="welsh-bullet-train.png"></a>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Hr64rglFs4w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## What does a shinkansen smell like ?


### Passenger window view
<iframe width="250" height="150" src="https://www.youtube.com/embed/ceqDG7t9O5c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Drivers Cabview

<iframe width="250" height="150" src="https://www.youtube.com/embed/xVxEQixjs40" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Station Fly Through
<iframe width="250" height="150" src="https://www.youtube.com/embed/Cm6TLNitR0M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



### Viaducts
<a target="_blank" href="https://www.google.co.uk/maps/@32.8301376,130.6951561,3a,63.2y,187.57h,93.7t/data=!3m6!1e1!3m4!1sgI7tfGLZiLR-LkMkOFtp8w!2e0!7i13312!8i6656"><img src="viaduct_1.png"></a>

<a target="_blank"  href="https://www.google.co.uk/maps/@32.6750628,130.6638618,3a,60y,218.31h,90.89t/data=!3m6!1e1!3m4!1ssa29my79m5ZZaKp0nGz0-A!2e0!7i13312!8i6656"><img src="viaduct_2.png"></a>


### Tunnels
<a target="_blank"  href="https://www.google.co.uk/maps/@31.6111536,130.4650069,3a,55.2y,40.21h,90.54t/data=!3m6!1e1!3m4!1sFktK4Szd1l5B9oIkDXeq-Q!2e0!7i13312!8i6656"><img src="tunnel_1.png"></a>
<a target="_blank"  href="https://www.google.co.uk/maps/@31.6478453,130.413702,3a,29y,124.24h,87.55t/data=!3m6!1e1!3m4!1sa2TgubH0wVuJCscF-iCHKA!2e0!7i13312!8i6656"><img src="tunnel_2.png"></a>


### Stations
<a target="_blank" href="https://www.google.co.uk/maps/@32.5178681,130.6340389,3a,75y,122.78h,86.62t/data=!3m6!1e1!3m4!1sUJn24WYiVjJGwxhTOQZ9yA!2e0!7i16384!8i8192"><img src="station_1.png"></a>
<a target="_blank" href="https://www.google.co.uk/maps/@31.5832981,130.5429969,3a,84.7y,314.82h,105.72t/data=!3m6!1e1!3m4!1slbhAWoMrxDUgvaZjYrAZ1w!2e0!7i16384!8i8192"><img src="station_2.png"></a>


