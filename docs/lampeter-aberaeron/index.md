Lampeter to Aberaeron Railway
=====================================

- Opened in 1911 and closed in 1965
- 13.7 miles
- See [wikipedia](https://en.wikipedia.org/wiki/Lampeter,_Aberayron_and_New_Quay_Light_Railway)
- Download <a href="https://welsh-railways.gitlab.io/lampeter-aberaeron/lampeter-aberaeron-railway.kml">KML File</a>

<a href="lampeter-aberaeron-railway.png"><img src="lampeter-aberaeron-railway.png"></a>


<iframe width="560" height="315" src="https://www.youtube.com/embed/TfotIu8Rt3A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



