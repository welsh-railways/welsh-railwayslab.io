Carmarthen to Llandeilo
=============================

Reinstatment of line (Beeching cuts) up the Tywi valley 

- Length: 15.2 Miles
- Download <a href="https://welsh-railways.gitlab.io/carmarthen-llandeilo/carmarthen-llandeilo-railway.kml">KML File</a>

<a href="carmarthen-llandeilo.png"><img src="carmarthen-llandeilo-railway.png"></a>


<iframe width="560" height="315" src="https://www.youtube.com/embed/NaCdtDrjPKo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>





