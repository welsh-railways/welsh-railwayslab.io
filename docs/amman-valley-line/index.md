Amman Valley Line
=============================

Reinstatment of olde line from Pantyfynnon/Ammanford up the Amman Valley to the Cwmllynfell plateau.

- Length: 9.2 Miles
- 6/7 new stations
- Download <a href="https://welsh-railways.gitlab.io/amman-valley-line/amman-valley-line.kml">KML File</a>
- [reddit/wales](https://www.reddit.com/r/Wales/comments/f9b9qw/new_amman_valley_line_reinstating_line_with_new/)
 

<a href="amman_valley_line.png"><img src="amman_valley_line.png"></a>


<iframe width="560" height="315" src="https://www.youtube.com/embed/6h1IiHQ8aJ4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


