# Welsh Railways

This is just some **fun playing** with maps and alike to see
what Wales would look with more railways. Do not take seriously!

This project is open source and hosted on gitlab

- [gitlab.com/welsh-railways](https://gitlab.com/welsh-railways)
- Please join in if you have ideas, info, comments et all


## Featured
* [New Mid Wales Line](mid-wales-line/) (north/south)

## Lines for Reinstatment & Reutilise ?

* [Anglesey Central](anglesey-central/)
* [Amman Valley Line](amman-valley-line/)
* [Carmarthen to Llandeilo](carmarthen-llandeilo/)
* [Neath Vale Railway](neath-vale/)

## Historic

* [Lampeter to Aberaeron](lampeter-aberaeron/)

## Mad Fantasies

* [Welsh Bullet Train](welsh-bullet/) (Japanese ShinKansen style)
* [Swansea Monorail](swansea-monorail/) (Monorail)
* [Cross Valley Line](cross-valleys-line/) (Tunneling heaven)

