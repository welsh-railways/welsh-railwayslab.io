Neath Vale Railway
=============================

Reinstatment of the line from:
 
- `Swansea Docks` to `Neath Junction` (1.5 miles) where its splits
  - for `Onllwyn` (11 miles)
  - or `Glynneath` (10 miles), and even `Aberdare` (+10 miles) and `Maesteg` (4 mile inc 3.5 mile tunnel)

- Length: 9.2 Miles
- 6/7 new stations
- [Wikipedia Page](https://en.wikipedia.org/wiki/Vale_of_Neath_Railway)
- Download <a href="https://welsh-railways.gitlab.io/neath-vale/neath-vale-railway.kml">KML File</a>

 

<a href="neath-vale-railway.png"><img src="neath-vale-railway.png"></a>


## Onllwyn Branch
<iframe width="560" height="315" src="https://www.youtube.com/embed/Vo-RlumEPyE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Glynneath Branch
<iframe width="560" height="315" src="https://www.youtube.com/embed/OiGC4Jv1v5k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



## Notes

### Glynneath to Hirwaun

- Glynneath at 60m / 200ft
- Hirwaun at 195m / 640ft
- thats 135 metres of elevation

<a href="glynneath-hirwaun-tunnel.png"><img src="glynneath-hirwaun.png"></a>


### Aberdare to Merthyr Tunnel

According to [Wikipedia](https://en.wikipedia.org/wiki/Vale_of_Neath_Railway#Construction), 
there was a mad idea to connect with a tunnel

- Aberdare at 125m / 410ft
- Merthyr at 160m / 530ft
- That's 35m over around four miles 

<a href="aberdare-merthyr-tunnel.png"><img src="aberdare-merthyr-tunnel.png"></a>

