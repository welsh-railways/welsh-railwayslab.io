Swansea Monorail
=========================

<s>Draft</s> Daft plan for an elevated monorail connecting **Swansea Railway Station** to:

- **Mumbles** - 5 miles
- **Enterprise Park** - 4.9 miles
- **Fabian Way** - 3.7 miles
- At say £30 million a mile thats about £410 million burnt
- Download <a href="https://welsh-railways.gitlab.io/swansea-monorail/swansea-monorail.kml">KML File</a> to cut some corners
 
<a href="swansea-monorail.png"><img height="300" src="swansea-monorail.png"></a>

## Mumbles to Enterprise Park
<iframe width="560" height="315" src="https://www.youtube.com/embed/JLmBeLUyhwo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Fabian Way
<iframe width="560" height="315" src="https://www.youtube.com/embed/8nplQwTqPwo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Japanese Examples

### Osaka Monorail

<iframe width="560" height="315" src="https://www.youtube.com/embed/8CHrlsPRMwQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Suspended Monorail

<iframe width="560" height="315" src="https://www.youtube.com/embed/JQQLUNMGw3Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Guideway

<iframe width="560" height="315" src="https://www.youtube.com/embed/LN-LiFx_sSA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

